package ru.fintech.homework

import android.app.Application
import ru.fintech.homework.di.provideGson
import ru.fintech.homework.di.provideNetworkChecker
import ru.fintech.homework.di.provideRemoteService
import ru.fintech.homework.di.provideRepository
import timber.log.Timber

class App : Application() {

    val gson by lazy(::provideGson)
    val repository by lazy(::provideRepository)
    val remoteService by lazy(::provideRemoteService)
    val networkChecker by lazy(::provideNetworkChecker)

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}