package ru.fintech.homework.ui.main

import ru.fintech.homework.data.Repository
import ru.fintech.homework.ui.base.BasePresenter
import timber.log.Timber

class MainPresenter<V : MainMvpView>(
    private val repository: Repository
) : BasePresenter<V>(), MainMvpPresenter<V> {

    override fun onPerformButtonClick(city: String) {
            getView()?.showLoading()
            compositeDisposable.add(repository
                .getWeather(city)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        getView()?.hideLoading()
                        getView()?.updateView(it)
                    }, { throwable ->
                        getView()?.hideLoading()
                        handleError(throwable)
                        Timber.d(throwable)
                    })
            )
    }
}