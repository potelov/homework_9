package ru.fintech.homework.ui.main

import ru.fintech.homework.data.model.WeatherDto
import ru.fintech.homework.ui.base.MvpView

interface MainMvpView : MvpView {

    fun updateView(weatherDto: WeatherDto)

}