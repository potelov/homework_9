package ru.fintech.homework.ui.main

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import ru.fintech.homework.R
import ru.fintech.homework.data.model.Weather
import ru.fintech.homework.data.model.WeatherDto
import ru.fintech.homework.di.providePresenter
import ru.fintech.homework.ui.base.BaseActivity
import ru.fintech.homework.utils.extensions.app

/**
 * Реализовать приложение, показывающее текущую погоду в городе из предложенного списка.
 * Часть 1. Подготавливаем окружение для взаимодействия с сервером.
 * 1) Сперва получаем ключ для разработчика (Достаточно зарегистрироваться на сайте, он бесплатный) инструкция: https://openweathermap.org/appid
 * <p>
 * 2) Выполнить 2 запроса для получения текущий погоды и прогноза одного из следующих городов:
 * Moscow,RU
 * Sochi,RU
 * Vladivostok,RU
 * Chelyabinsk,RU
 * API запроса By city name можно прочитать тут:
 * https://openweathermap.org/current#name
 * <p>
 * 1) Шаблон запроса на текущую погоду: api.openweathermap.org/data/2.5/weather?q={city name},{country code}
 * Пример: http://api.openweathermap.org/data/2.5/weather?q=Moscow,ru&APPID=7910f4948b3dcb251ebc828f28d8b30b
 * <p>
 * 2) Шаблон запроса на прогноз погоды: api.openweathermap.org/data/2.5/forecast?q={city name},{country code}
 * Пример: http://api.openweathermap.org/data/2.5/forecast?q=Moscow,ru&APPID=7910f4948b3dcb251ebc828f28d8b30b
 * <p>
 * Важно: Данные с сервера должны приходить в json формате (прим.: значение температуры в градусах Цельсия). Также можно добавить локализацию языка: https://openweathermap.org/current#other
 * <p>
 * Часть 2. Разработка мобильного приложения.
 * Шаблон проекта находиться в ветке: homework_9_network
 * UI менять не надо, используем уже реализованные методы MainActivity.
 * Написать код выполнения запроса в методе performRequest(@NonNull String city).
 * <p>
 * Реализовать следующий функционал:
 * a) С помощью Retrofit, Gson и других удобных для вас инструментов, написать запросы для получения текущий и прогнозы погоды в конкретном городе, используя метод API By city name.
 * б) Реализовать JsonDeserializer, который преобразует json структуру пришедшую с сервера в модель Weather (Также и для прогноза погоды). в) Во время загрузки данных показывать прогресс бар, в случае ошибки выводить соотвествующее сообщение.
 * г) Если у пользователя нет доступа в интернет, кнопка выполнить запрос не активна. При его появлении/отсутствии необходимо менять состояние кнопки;
 * д) (Дополнительное задание) Улучшить форматирование вывода данных на свое усмотрение, текущий погоды и прогноза. Оценивается UI интерфейс.
 */

class MainActivity : BaseActivity(), MainMvpView {

    private val presenter by lazy { providePresenter() }

    private lateinit var spinner: Spinner
    private lateinit var performBtn: Button
    private lateinit var resultTextView: TextView
    private val networkStateReceiver = NetworkStateReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onAttach(this)
        init()
    }

    private fun init() {
        spinner = findViewById(R.id.spinner)
        performBtn = findViewById(R.id.performBtn)
        resultTextView = findViewById(R.id.resultTextView)
        performBtn.setOnClickListener { performRequest(spinner.selectedItem.toString()) }
        val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(networkStateReceiver, intentFilter)
    }

    @SuppressLint("DefaultLocale")
    private fun printResult(weather: Weather, forecast: List<Weather>) {
        val stringBuilder = StringBuilder()
        stringBuilder.append(
            String.format(
                "CurrentWeather\nDesc: %s \nTimeUnix: %d\nTemp: %.1f\nSpeed wind: %.1f",
                weather.description,
                weather.time,
                weather.temp,
                weather.speedWind
            )
        )

        if (!forecast.isEmpty()) {
            val firstForecastWeather = forecast[0]
            stringBuilder.append("\n")
            stringBuilder.append(
                String.format(
                    "Forecast\nDesc: %s \nTimeUnix: %d\nTemp: %.1f\nSpeed wind: %.1f",
                    firstForecastWeather.description,
                    firstForecastWeather.time,
                    firstForecastWeather.temp,
                    firstForecastWeather.speedWind
                )
            )
        }
        resultTextView.text = stringBuilder.toString()
    }

    private fun setEnablePerformButton(enable: Boolean) {
        performBtn.isEnabled = enable
    }


    private fun performRequest(city: String) {
        presenter.onPerformButtonClick(city)
    }

    override fun updateView(weatherDto: WeatherDto) {
        printResult(weatherDto.weather, weatherDto.forecast)
        view_animation.setAnimation(R.raw.cloud)
        view_animation.playAnimation()

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
        unregisterReceiver(networkStateReceiver)

    }

    private inner class NetworkStateReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val isDeviceOnline = app.networkChecker.isNetworkAvailable()
            if (isDeviceOnline) {
                setEnablePerformButton(true)
            } else {
                setEnablePerformButton(false)
            }
        }
    }
}
