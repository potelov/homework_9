package ru.fintech.homework.data

import io.reactivex.Single
import io.reactivex.functions.BiFunction
import ru.fintech.homework.data.model.Weather
import ru.fintech.homework.data.model.WeatherDto
import ru.fintech.homework.data.remote.RemoteService

class RepositoryImpl constructor(private val remoteService: RemoteService) : Repository {

    override fun getForecastApiCall(city: String): Single<Weather> {
        return remoteService.getForecastApiCall(city)
    }

    override fun getWeatherApiCall(city: String): Single<Weather> {
        return remoteService.getWeatherApiCall(city)
    }

    override fun getWeather(city: String): Single<WeatherDto> {
        return Single.zip(
            getWeatherApiCall(city),
            getForecastApiCall(city),
            BiFunction { weather, forecast  -> WeatherDto(weather, forecast.forecast) })
    }
}