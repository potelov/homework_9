package ru.fintech.homework.data

import io.reactivex.Single
import ru.fintech.homework.data.model.WeatherDto
import ru.fintech.homework.data.remote.RemoteService

interface Repository : RemoteService {

    fun getWeather(city: String): Single<WeatherDto>
}