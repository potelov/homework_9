package ru.fintech.homework.data.model

data class WeatherDto(val weather: Weather, val forecast: List<Weather>)
