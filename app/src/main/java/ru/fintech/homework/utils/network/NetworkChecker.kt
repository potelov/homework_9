package ru.fintech.homework.utils.network

import android.app.Application
import ru.fintech.homework.utils.extensions.systemConnectivityManager

interface NetworkChecker {

    fun isNetworkAvailable(): Boolean

}

class NetworkCheckerImpl(app: Application) : NetworkChecker {

    private val connectivityManager = app.systemConnectivityManager

    override fun isNetworkAvailable(): Boolean =
        connectivityManager.activeNetworkInfo?.isConnected ?: false

}