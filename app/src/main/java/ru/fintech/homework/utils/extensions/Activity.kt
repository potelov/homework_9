package ru.fintech.homework.utils.extensions

import android.app.Activity
import ru.fintech.homework.App

val Activity.app get() = application as App


