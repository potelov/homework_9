package ru.fintech.homework.utils.extensions

import android.content.Context
import android.net.ConnectivityManager

val Context.systemConnectivityManager: ConnectivityManager
    get() = getAndroidSystemService(Context.CONNECTIVITY_SERVICE)

private inline fun <reified T> Context.getAndroidSystemService(name: String) =
    getSystemService(name) as T