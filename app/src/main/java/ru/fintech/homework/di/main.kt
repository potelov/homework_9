package ru.fintech.homework.di

import ru.fintech.homework.ui.main.MainActivity
import ru.fintech.homework.ui.main.MainMvpPresenter
import ru.fintech.homework.ui.main.MainMvpView
import ru.fintech.homework.ui.main.MainPresenter
import ru.fintech.homework.utils.extensions.app

fun MainActivity.providePresenter(): MainMvpPresenter<MainMvpView> {
    return MainPresenter(app.repository)
}