package ru.fintech.homework.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.fintech.homework.App
import ru.fintech.homework.BuildConfig
import ru.fintech.homework.data.Repository
import ru.fintech.homework.data.RepositoryImpl
import ru.fintech.homework.data.model.Weather
import ru.fintech.homework.data.remote.RemoteService
import ru.fintech.homework.data.remote.WeatherModelDeserializer
import ru.fintech.homework.utils.APP_ID
import ru.fintech.homework.utils.BASE_URL
import ru.fintech.homework.utils.DATE_FORMAT
import ru.fintech.homework.utils.network.NetworkChecker
import ru.fintech.homework.utils.network.NetworkCheckerImpl

fun App.provideNetworkChecker(): NetworkChecker =
    NetworkCheckerImpl(this)

fun App.provideRepository(): Repository =
    RepositoryImpl(remoteService)

fun App.provideRemoteService(): RemoteService =
    Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(
            OkHttpClient.Builder()
                .addQueryParam(APP_ID)
                .setDebugEnabled(BuildConfig.DEBUG)
                .build()
        )
        .build()
        .create(RemoteService::class.java)

fun provideGson(): Gson =
    GsonBuilder()
        .registerTypeAdapter(Weather::class.java, WeatherModelDeserializer())
        .setDateFormat(DATE_FORMAT)
        .create()

private fun OkHttpClient.Builder.setDebugEnabled(debugEnabled: Boolean) =
    apply {
        if (debugEnabled) {
            addNetworkInterceptor(HttpLoggingInterceptor().also {
                it.level = HttpLoggingInterceptor.Level.BODY
            })
        }
    }

private fun OkHttpClient.Builder.addQueryParam(appId: String) =
    apply {
        addInterceptor { chain ->
            chain.proceed(
                chain.request()
                    .newBuilder()
                    .url(chain.request().url().newBuilder().addQueryParameter("APPID", appId).build())
                    .build()
            )
        }

    }

